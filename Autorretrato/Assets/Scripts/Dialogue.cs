using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{
    bool isPlayerInRange;
    [SerializeField] GameObject mark;
    [SerializeField] GameObject dialoguePanel;
    [SerializeField] TMP_Text text;
    [SerializeField, TextArea(4,6)] string[] dialogueLines;

    [SerializeField] float typingTime = 0.05f;

    bool didDialogueStart;
    int lineIndex;

    void Update()
    {   
        if(isPlayerInRange && Input.GetButtonDown("Fire1"))
        {
            if(!didDialogueStart)
            {
                StartDialogue();
            }
            else if(text.text == dialogueLines[lineIndex])
            {
                NextDialogueLine();
            }
            else 
            {
                StopAllCoroutines();
                text.text = dialogueLines[lineIndex];
            }
        }
    }

    IEnumerator ShowLine()
    {
        text.text = string.Empty;

        foreach (char c in dialogueLines[lineIndex])
        {
            text.text += c;
            yield return new WaitForSecondsRealtime(typingTime);
        }
    }

    void StartDialogue()
    {
        didDialogueStart = true;
        dialoguePanel.SetActive(true);
        mark.SetActive(false);
        lineIndex = 0;
        Time.timeScale = 0f;
        StartCoroutine(ShowLine());
    }

    void NextDialogueLine()
    {
        lineIndex++;
        if(lineIndex<dialogueLines.Length)
        {
           StartCoroutine(ShowLine());
        }
        else 
        {
            didDialogueStart = false;
            dialoguePanel.SetActive(false);
            mark.SetActive(true);
            Time.timeScale = 1f;
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            mark.SetActive(true);
            isPlayerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            mark.SetActive(false);
            isPlayerInRange = false;
        }
    }
}
