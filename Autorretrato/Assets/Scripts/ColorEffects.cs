using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorEffects : MonoBehaviour
{
   #region variables de estetica
    public SpriteRenderer sprite;
    public Color[] colors;
    int currentColorI = 0;
    int targetColorI = 1;
    float targetPoint;
    //public float time;
    #endregion


    void Update()
    {
        ColorLogic();
    }

    void ColorLogic() 
    {
        targetPoint += Time.deltaTime;
        sprite.color = Color.Lerp(colors[currentColorI], colors[targetColorI], targetPoint);
        if(targetPoint >= 1f)
        {
            targetPoint = 0f;
            currentColorI = targetColorI;
            targetColorI++;

            if(targetColorI == colors.Length)
                targetColorI = 0;
        }
    }

}
