using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    //[SerializeField] SpotLight spotLight;
    [SerializeField] Light2D light2D;
    public TMP_Text txt;

    int points;
    float _intensity = 0;
     
    void Awake()
    {
        
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
        //spotLight = GetComponent<SpotLight>();
    }

    void Start() 
    {
        light2D.intensity = 0;
    }

     
    void Update()
    {

        SpotLightLogic();
    }

    void SpotLightLogic()
    {
        light2D.intensity = _intensity;

        if(points>=15) 
        {
            light2D.intensity++;
            if(light2D.intensity>=3) 
            {
                SceneManager.LoadScene("Final");
            }
        }
    }

    public void AddPoints()
    {
        Debug.Log(points);
        points++;
        _intensity += 0.1f;
        //spotLight.sphereRadius--;

    }

    public void RemovePoints()
    {
        Debug.Log(points);
        points--;
        _intensity -= 0.1f;
        //spotLight.sphereRadius--;

    }
}
