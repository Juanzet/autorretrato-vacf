using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EBT : MonoBehaviour
{
    [SerializeField] TMP_Text txt;
    

    void Start() 
    {
       txt = GameManager.Instance.txt;  
    }
    public void RandomPhrase()
    {
        int r = Random.Range(0,1);

        if(r == 0) 
        {
            txt.text = "Sos una mala persona";
        }

         if(r == 1) 
        {
            txt.text = "Vas a fracasar";
        }
    }


    private void OnTriggerEnter2D(Collider2D other) 
    {   
        if(other.gameObject.CompareTag("Player")) 
        {
            RandomPhrase();
            GameManager.Instance.RemovePoints();
            Destroy(this.gameObject);
        }
        
    }

}
