using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTSpawn : MonoBehaviour
{
    [SerializeField] GameObject btSpawn;
    [SerializeField] GameObject ebtSpawn;
    [SerializeField] Transform p1;
    [SerializeField] Transform p2;
    [SerializeField] float btInterval;

    void Start()
    {
        StartCoroutine(SpawnBT(btInterval, btSpawn));
        StartCoroutine(SpawnBT(btInterval, ebtSpawn));
    }

    IEnumerator SpawnBT(float interval, GameObject bt)
    {
        yield return new WaitForSeconds(interval);
        RandomPosition();   
        ERandomPosition();
        GameObject newBT = Instantiate(bt, new Vector3(Random.Range(-5, 5),Random.Range(-6, 6), 0),Quaternion.identity);
        //GameObject newBT = Instantiate(bt, new Vector3(Random.Range(-4, 4),Random.Range(-8, 8), 0),Quaternion.identity);
        StartCoroutine(SpawnBT(interval, bt));
    }
     

    void RandomPosition() 
    {
        int random = Random.Range(-5,5);

        if(random<=0) 
        {
            Instantiate(btSpawn, p1);
        }

        if(random<=5) 
        {
            Instantiate(btSpawn, p2);
        }
    }

    void ERandomPosition() 
    {
        int random = Random.Range(-5,5);

        if(random<=0) 
        {
            Instantiate(ebtSpawn, p1);
        }

        if(random<=5) 
        {
            Instantiate(ebtSpawn, p2);
        }
    }
}
